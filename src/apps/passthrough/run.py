# Copyright 2014 Yasha Bubnov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

import netasm.core.lang
import netasm.core.inst
import netasm.utils.parser
import netasm.utils.emulator
from . import code


h0 = netasm.utils.parser.gen_hdr({
    "inport": 1,
    "outport": 0,
    "srcmac": 1234,
    "dstmac": 4321,
    "i": 0
    })

h1 = netasm.utils.parser.gen_hdr({
    "inport": 2,
    "outport": 0,
    "srcmac": 6543,
    "dstmac": 5432,
    "i": 0
    })

c0 = netasm.core.inst.WRT(code.mt_tbl, {"inport": 1}, 0)
c1 = netasm.core.inst.WRT(code.md_tbl, {"outport": 2}, 0)
c2 = netasm.core.inst.WRT(code.mt_tbl, {"inport": 2}, 1)
c3 = netasm.core.inst.WRT(code.md_tbl, {"outport": 1}, 1)

il = [c0, c1, c2, c3, h0, h1]

e = netasm.utils.emulator.Emulator(code.ic, il, code.tc)
for h in e.run(): print(h)

