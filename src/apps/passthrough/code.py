# Copyright 2014 Yasha Bubnov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

import netasm.core.lang
import netasm.core.inst


tbl_size = 2

mt_size = tbl_size
mt_tbl = netasm.core.lang.Table("mt0", mt_size, ["inport"])
mt_val = [{"inport": 0}]*tbl_size

md_size = tbl_size
md_tbl = netasm.core.lang.Table("md0", md_size, ["outport"])
md_val = [{"outport": 0}]*tbl_size

ic = [netasm.core.inst.MKT(mt_tbl, mt_val)
    , netasm.core.inst.MKT(md_tbl, md_val)]

tc = [netasm.core.inst.IBRTF(mt_tbl, "i", "l_miss")
    , netasm.core.inst.LDFTF(md_tbl, "i")
    , netasm.core.inst.JMP("l_end")
    , netasm.core.inst.LBL("l_miss")
    , netasm.core.inst.DRP()
    , netasm.core.inst.LBL("l_end")
    , netasm.core.inst.HLT()]

