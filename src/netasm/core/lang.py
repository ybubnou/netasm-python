# Copyright 2014 Yasha Bubnov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

from . import constants as _core_constants
from . import errors as _core_errors

class Op(object):
    Add = staticmethod(lambda x, y: x + y)
    Sub = staticmethod(lambda x, y: x - y)
    And = staticmethod(lambda x, y: x & y)
    Or = staticmethod(lambda x, y: x | y)
    Xor = staticmethod(lambda x, y: x ^ y)

class CmpOp(object):
    Eq = staticmethod(lambda x, y: x == y)
    Neq = staticmethod(lambda x, y: x != y)
    Lt = staticmethod(lambda x, y: x < y)
    Gt = staticmethod(lambda x, y: x > y)
    Le = staticmethod(lambda x, y: x <= y)
    Ge = staticmethod(lambda x, y: x >= y)


class State(object):
    def __init__(self, header=None):
        self._header = header or {}
        self._regs = {}
        self._tables = {}

    header = property(lambda self: self._header)
    regs = property(lambda self: self._regs)
    tables = property(lambda self: self._tables)

    def _validate_table(self, table_name):
        if not table_name in self._tables:
            err_msg = "Invalid table '%s', use MKT to create table"
            raise _core_errors.NetASMCoreException(err_msg % table_name)

    def _validate_reg(self, reg):
        if not reg in self._regs:
            err_msg = "Invalid register '%s'. Use MKR to create the register."
            raise _core_errors.NetASMCoreException(err_msg % reg)

    def read_PID(self):
        inst_id, valid = self._header.get("PID")
        return inst_id

    def update_PID(self, value=1):
        inst_id, valid = self._header.get("PID")
        self._header["PID"] = (inst_id+value, valid)

    def read_header(self, field):
        if field in _core_constants.RESERVED_FIELDS:
            err_msg = "Cannot operate on special field '%s'."
            raise _core_errors.NetASMCoreException(err_msg % field)
        elif field in self._header:
            return self._header.get(field)
        else:
            err_msg = "Invalid field '%s'. Use ADD to add header field."
            raise _core_errors.NetASMCoreException(err_msg % field)

    def update_header(self, header):
        self._header.update(header)

    def create_reg(self, reg, value):
        if reg in self._regs:
            err_msg = "Register '%s' already created."
            raise _core_errors.NetASMCoreException(err_msg % reg)
        self._regs.setdefault(reg, value)

    def update_reg(self, reg, value):
        self._validate_reg(reg)
        self._regs[reg] = value

    def read_reg(self, reg):
        self._validate_reg(reg)
        return self._regs[reg]

    def create_table(self, table_name, table, records):
        if table_name in self._tables:
            err_msg = "Table '%s' already created."
            raise _core_errors.NetASMCoreException(err_msg % table_name)
        self._tables.setdefault(table_name, table)
        self._tables.get(table_name).create(records)

    def read_table(self, table_name, row=None):
        self._validate_table(table_name)
        if not row is None:
            return self._tables.get(table_name).read(row)
        return self._tables.get(table_name)

    def update_table(self, table_name, record, row):
        self._validate_table(table_name)
        self._tables[table_name].update(record, row)

    def match_table(self, table_name, record):
        self._validate_table(table_name)
        return self._tables[table_name].match(record)


class Table(object):
    """Table collection mapping name to tuple (table, pattern)."""
    def __init__(self, name, tbl_size, fields):
        super(Table, self).__init__()
        self._name = name
        self._fields = tuple(fields)
        self._tbl_size = tbl_size
        self._table = []

    name = property(lambda self: self._name)
    fields = property(lambda self: self._fields)

    def _validate_fields(self, fields):
        if self._fields != tuple(fields):
            err_msg = "Pattern '%s' doesn't comply with table pattern '%s'."
            raise _core_errors.NetASMCoreException(
                    err_msg % (self._fields, tuple(fields)))

    def _validate_capacity(self, row):
        if row > self._tbl_size:
            err_msg = "Table index '%s' out of range [0..%s], " \
                        "use MKT to create the table."
            raise _core_errors.NetASMCoreException(
                    err_msg % (row, self._tbl_size))

    def create(self, records):
        fields = records[0].keys()
        self._validate_fields(fields)
        for row, value in zip(xrange(self._tbl_size), records):
            self._table.insert(row, value)

    def read(self, row):
        self._validate_capacity(row)
        return self._table[row]

    def update(self, record, row):
        self._validate_fields(record.keys())
        self._validate_capacity(row)
        self._table.insert(row, record)

    def match(self, record):
        try:
            return True, self._table.index(record)
        except ValueError:
            return False, None

