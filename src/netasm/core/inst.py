# Copyright 2014 Yasha Bubnov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

from . import errors as _core_errors

class BaseInstruction(object):
    def __init__(self):
        super(BaseInstruction, self).__init__()

    def _fetch_header(self, fields, state):
        record = {}
        for field in fields:
            field_val, _ = state.read_header(field)
            record.setdefault(field, field_val)
        return record

    def invoke(self, state):
        state.update_PID()
        return state

class InitializeInstruction(BaseInstruction):
    def __init__(self):
        super(InitializeInstruction, self).__init__()

class TopologyInstruction(BaseInstruction):
    def __init__(self):
        super(TopologyInstruction, self).__init__()

    def _label_id(self, code, label):
        for inst in code:
            if hasattr(inst, "label") and inst.label == label:
                return code.index(inst)
        return -1

class JumpInstruction(TopologyInstruction):
    def _jump_to(self, code, state, label):
        inst_id = state.read_PID()
        label_id = self._label_id(code, self._label)
        if label_id > inst_id:
            value = label_id - inst_id
            state.update_PID(value)
        else:
            err_msg = "Only forward jumps are allowed. Jump to '%s' failed."
            raise Exception(err_msg % label)
        return state

class ControlInstruction(BaseInstruction):
    def __init__(self):
        super(ControlInstruction, self).__init__()

class MKR(InitializeInstruction):
    """Make a new register (reg) with default value (value)."""
    def __init__(self, reg, value):
        super(MKR, self).__init__()
        self._reg = reg
        self._value = value

    def invoke(self, state):
        """Create register with default value"""
        state.create_reg(self._reg, self._value)
        return state

class MKT(InitializeInstruction):
    """Make a new dynamic table (table) and load it
       with content from dictionary (records)."""
    def __init__(self, table, records):
        super(MKT, self).__init__()
        self._table = table
        self._records = records

    def invoke(self, state):
        """Create dynamic table"""
        state.create_table(self._table.name, self._table, self._records)
        return state


class WRR(ControlInstruction):
    """Write register (reg) with value (value)."""
    def __init__(self, reg, value):
        super(WRR, self).__init__()
        self._reg = reg
        self._value = value

    def invoke(self, state):
        state.update_reg(self._reg, self._value)
        return state

class WRT(ControlInstruction):
    """"Write table (table) with pattern (pattern) at index value (value)."""
    def __init__(self, table, pattern, value):
        super(WRT, self).__init__()
        self._table_name = table.name
        self._pattern = pattern
        self._value = value

    def invoke(self, state):
        state.update_table(self._table_name, self._pattern, self._value)
        return state


class HLT(TopologyInstruction):
    """End of code."""
    def __init__(self):
        super(HLT, self).__init__()

    def invoke(self, code, state):
        raise _core_errors.NetASMHaltException

class LBL(TopologyInstruction):
    """Label (label) for jump and branch instructions."""
    def __init__(self, label):
        super(LBL, self).__init__()
        self._label = label

    label = property(lambda self: self._label)

    def invoke(self, code, state):
        state = super(LBL, self).invoke(state)
        return state


class LDR(TopologyInstruction):
    """Load register (reg) with value (value)."""
    def __init__(self, reg, value):
        super(LDR, self).__init__()
        self._reg = reg
        self._value = value

    def invoke(self, code, state):
        state = super(LDR, self).invoke(state)
        state.update_reg(self._reg, self._value)
        return state

class LDRR(TopologyInstruction):
    """Load register (dreg) with register (sreg)."""
    def __init__(self, dreg, sreg):
        super(LDRR, self).__init__()
        self._dreg = dreg
        self._sreg = sreg

    def invoke(self, code, state):
        state = super(LDRR, self).invoke(state)
        sreg_val = state.read_reg(self._sreg)
        state.update_reg(self._dreg, sreg_val)
        return state

class LDRF(TopologyInstruction):
    """Load register (reg) with field (field)."""
    def __init__(self, reg, field):
        super(LDRF, self).__init__()
        self._reg = reg
        self._field = field

    def invoke(self, code, state):
        state = super(LDRF, self).invoke(state)
        field_val, _ = state.read_header(self._field)
        state.update_reg(self._reg, field_val)
        return state

class OPR(TopologyInstruction):
    """Apply operation (op) on register (reg1) and value (value)
    and store it in register (reg0)."""
    def __init__(self, reg0, reg1, op, value):
        super(OPR, self).__init__()
        self._reg0 = reg0
        self._reg1 = reg1
        self._op = op
        self._value = value

    def invoke(self, code, state):
        state = super(OPR, self).invoke(state)
        reg1_val = state.read_reg(self._reg1)
        reg0_val = self._op(reg1_val, self._value)
        state.update_reg(self._reg0, reg0_val)
        return state

class OPRR(TopologyInstruction):
    """Apply operation (op) on register (reg1) and
    register (reg2) and store it in register (reg0)."""
    def __init__(self, reg0, reg1, op, reg2):
        super(OPRR, self).__init__()
        self._reg0 = reg0
        self._reg1 = reg1
        self._reg2 = reg2
        self._op = op

    def invoke(self, code, state):
        state = super(OPRR, self).invoke(state)
        reg1_val = state.read_reg(self._reg1)
        reg2_val = state.read_reg(self._reg2)
        reg0_val = self._op(reg1_val, reg2_val)
        state.update_reg(self._reg0, reg0_val)
        return state

class OPRF(TopologyInstruction):
    """Apply operation (op) on register (reg1) and field (feild)
    and store it in register (reg0)."""
    def __init__(self, reg0, reg1, op, field):
        super(OPRF, self).__init__(self)
        self._reg0 = reg0
        self._reg1 = reg1
        self._op = op
        self._field = field

    def invoke(self, code, state):
        state = super(OPRF, self).invoke(state)
        reg1_val = state.read_reg(self._reg1)
        field_val, _ = state.read_header(self._field)
        reg0_val = self._op(reg1_val, field_val)
        state.update_reg(self._reg0, reg0_val)
        return state

class LDTP(TopologyInstruction):
    """Load table with pattern (field, value) at value: load table (table)
    with pattern (pattern) at index in value (value)."""
    def __init__(self, table, pattern, value):
        super(LDTP, self).__init__()
        self._table_name = table.name
        self._pattern = pattern
        self._value = value

    def invoke(self, code, state):
        state = super(LDTP, self).invoke(state)
        index = self._value
        state.update_table(self._table_name, self._pattern, index)
        return state

class LDTPR(TopologyInstruction):
    """Load table (table) with pattern (pattern) at index register (reg)."""
    def __init__(self, table, pattern, reg):
        super(LDTPR, self).__init__()
        self._table_name = table.name
        self._pattern = pattern
        self._reg = reg

    def invoke(self, code, state):
        state = super(LDTPR, self).invoke(state)
        index = state.read_reg(self._reg)
        state.update_table(self._table_name, self._pattent, index)
        return state

class LDTPF(TopologyInstruction):
    """Load table with pattern (field, value) at field: load table (table)
    with pattern (pattern) at index field (field)."""
    def __init__(self, table, pattern, field):
        super(LDTPF, self).__init__()
        self._table_name = table.name
        self._pattern = pattern
        self._field = field

    def invoke(self, code, state):
        state = super(LDTPF, self).invoke(state)
        index, _ = state.read_header(self._field)
        state.update_table(self._table_name, self._pattern, index)
        return state

class LDTF(TopologyInstruction):
    """Load table (table) with pattern (fields) at index value (value)."""
    def __init__(self, table, fields, value):
        super(LDTF, self).__init__()
        self._table_name = table.name
        self._fields = fields
        self._value = value

    def invoke(self, code, state):
        state = super(LDTF, self).invoke(state)
        pattern, index = {}, self._value
        for field, value in self._fields.iteritems():
            field_val, _ = state.read_header(value)
            pattern.setdefault(field, field_val)
        state.update_table(self._table_name, pattern, index)

class LDTFR(TopologyInstruction):
    """Load table (table) with pattern (fields) at index register (reg)."""
    def __init__(self, table, fields, reg):
        super(LDTFR, self).__init__()
        self._table_name = table.name
        self._fields = fields
        self._reg = reg

    def invoke(self, code, state):
        state = super(LDTFR, self).invoke(state)
        pattern, index= {}, state.read_reg(self._reg)
        for field, value in self._fields.iteritems():
            field_val, _ = state.read_header(value)
            pattern.setdefault(field, field_val)
        state.update_table(self._table_name, pattern, index)
        return state

class LDTFF(TopologyInstruction):
    """Load table (table) with pattern (fields) at index field (field)."""
    def __init__(self, table, fields, field):
        super(LDTFF, self).__init__()
        self._table_name = table.name
        self._fields = fields
        self._field = field

    def invoke(self, code, state):
        state = super(LDTFF, self).invoke(state)
        pattern = {}
        index, _ = state.read_header(self._field)
        for field, value in self._fields.iteritems():
            field_val, _ = state.read_header(value)
            pattern.setdefault(field, field_val)
        state.update_table(self._table_name, pattern, index)

class ID(TopologyInstruction):
    """Pass the incoming state as is to the next instruction."""
    def __init__(self):
        super(ID, self).__init__()

    def invoke(self, code, state):
        state = super(ID, self).invoke(state)
        return state

class DRP(TopologyInstruction):
    """Set the "DRP" field in the header to 1 (i.e., True)."""
    def __init__(self):
        super(DRP, self).__init__()

    def invoke(self, code, state):
        state = super(DRP, self).invoke(state)
        state.update_header({"DRP": (1, True)})
        return state

class ADD(TopologyInstruction):
    """Set header field (field) to 1 i.e., add field in the header."""
    def __init__(self, field):
        super(ADD, self).__init__()
        self._field = field

    def invoke(self, code, state):
        state = super(ADD, self).invoke(state)
        field_val, _ = state.read_header(self._field)
        state.update_header({self._field: (field_val, True)})
        return state

class DEL(TopologyInstruction):
    """Set header field (field) to 0 i.e., delete field from the header."""
    def __init__(self, field):
        super(DEL, self).__init__()
        self._field = field

    def invoke(self, code, state):
        state = super(DEL, self).invoke(state)
        field_val, _ = state.read_header(self._field)
        state.update_header({self._field: (field_val, False)})
        return state

class LDF(TopologyInstruction):
    """Load header field (field) with the value (value)."""
    def __init__(self, field, value):
        super(LDF, self).__init__()
        self._field = field
        self._value = value

    def invoke(self, code, state):
        state = super(LDF, self).invoke(state)
        _, validity = state.read_header(self._field)
        state.update_header({self._field: (self._value, validity)})
        return state

class LDFR(TopologyInstruction):
    """Load header field (field) with the register (reg)."""
    def __init__(self, field, reg):
        super(LDFR, self).__init__()
        self._field = field
        self._reg = reg

    def invoke(self, code, state):
        state = super(LDFR, self).invoke(state)
        reg_val = state.read_reg(self._reg)
        _, validity = state.read_header(self._field)
        state.update_header({self._field: (reg_val, validity)})
        return state

class LDFF(TopologyInstruction):
    """Load header field (field0) with the field (field1)."""
    def __init__(self, field0, field1):
        super(LDFF, self).__init__()
        self._field0 = field0
        self._field1 = field1

    def invoke(self, code, state):
        state = super(LDFF, self).invoke(state)
        field_val, _ = state.read_header(self._field1)
        _, validity = state.read_header(self._field0)
        state.update_header({self._field0: (field_val, validity)})

class LDFT(TopologyInstruction):
    """Load header with the table (table) at index value (value)."""
    def __init__(self, table, value):
        super(LDFT, self).__init__()
        self._table_name = table.name
        self._value = value

    def invoke(self, code, state):
        state = super(LDFT, self).invoke(state)
        header_val = state.read_table(self._table_name, self._value)
        state.update_header(header_val)
        return state

class LDFTR(TopologyInstruction):
    """Load eader with the table (table) at index register (reg)."""
    def __init__(self, table, reg):
        super(LDFTR, self).__init__()
        self._table_name = table.name
        self._reg = reg

    def invoke(self, code, state):
        state = super(LDFTR, self).invoke(state)
        reg_val = state.read_reg(self._reg)
        header_val = state.read_table(self._table_name, reg_val)
        state.update_header(header_val)
        return state

class LDFTF(TopologyInstruction):
    """Load header with the table (table) at index field (field)."""
    def __init__(self, table, field):
        super(LDFTF, self).__init__()
        self._table_name = table.name
        self._field = field

    def invoke(self, code, state):
        state = super(LDFTF, self).invoke(state)
        field_val, _ = state.read_header(self._field)
        header_val = state.read_table(self._table_name, field_val)
        record = dict([(f, (v, True)) for f, v in header_val.iteritems()])
        state.update_header(record)
        return state

class OPF(TopologyInstruction):
    """Apply operation (op) on field (field1) and value (value)
    and store it in field (field0)."""
    def __init__(self, field0, field1, op, value):
        super(OPF, self).__init__()
        self._field0 = field0
        self._field1 = field1
        self._op = op
        self._value = value

    def invoke(self, code, state):
        state = super(OPF, self).invoke(state)
        field_val, _ = state.read_header(self._field1)
        _, validity = state.read_header(self._field0)
        field_val = self._op(field_val, self._value)
        state.update_header({self._field0: (field_val, validity)})
        return state

class OPFR(TopologyInstruction):
    """Apply operation (op) on field (field1) and register (reg)
    and store it in field (field0)."""
    def __init__(self, field0, field1, op, reg):
        super(OPFR, self).__init__()
        self._field0 = field0
        self._field1 = field1
        self._op = op
        self._reg = reg

    def invoke(self, code, state):
        state = super(OPFR, self).invoke(state)
        reg_val = state.read_reg(self._reg)
        field_val, _ = state.read_header(self._field1)
        _, validity = state.read_header(self._field0)
        field_val = self._op(field_val, reg_val)
        state.update_header({self._field0: (field_val, validity)})
        return state

class OPFF(TopologyInstruction):
    """Apply operation (op) on field (field1) and field (field2)
    and store it in field (field0)."""
    def __init__(self, field0, field1, op, field2):
        super(OPFF, self).__init__()
        self._field0 = field0
        self._field1 = field1
        self._field2 = field2
        self._op = op

    def invoke(self, code, state):
        state = super(OPFF, self).invoke(state)
        field1_val, _ = state.read_header(self._field1)
        field2_val, _ = state.read_header(self._field2)
        field0_val = self._op(field1_val, field2_val)
        _, validity = state.read_header(self._field0)
        state.update_header({self._field0: (field0_val, validity)})
        return state

class JMP(JumpInstruction):
    """Jump to fixed value PID."""
    def __init__(self, label):
        super(JMP, self).__init__()
        self._label = label

    def invoke(self, code, state):
        state = super(JMP, self).invoke(state)
        return self._jump_to(code, state, self._label)

class BRR(JumpInstruction):
    """Branch control to label (label) if the result of the comparison
    on register (reg) and value (val) is true."""
    def __init__(self, reg, cmp_op, value, label):
        super(BRR, self).__init__()
        self._reg = reg
        self._cmp_op = cmp_op
        self._value = value
        self._label = label

    def invoke(self, code, state):
        state = super(BRR, self).invoke(state)
        reg_val = state.read_reg(self._reg)
        cmp_val = self._cmp_op(reg_val, self._value)
        if cmp_val:
            state = self._jump_to(code, state, self._label)
        return state

class BRRR(JumpInstruction):
    """Branch control to label (label) if the result of the comparison
    on register (reg0) and register (reg1) is true."""
    def __init__(self, reg0, cmp_op, reg1, label):
        super(BRRR, self).__init__()
        self._reg0 = reg0
        self._reg1 = reg1
        self._cmp_op = cmp_po
        self._label = label

    def invoke(self, code, state):
        state = super(BRRR, self).invoke(state)
        reg0_val = state.read_reg(self._reg0)
        reg1_val = state.read_reg(self._reg1)
        cmp_val = self._cmp_op(reg0_val, reg1_val)
        if cmp_val:
            state = self._jump_to(code, state, self._label)
        return state


class BRRF(JumpInstruction):
    """Branch control to label (label) if the result of the comparison
    on register (reg) and field (field) is true."""
    def __init__(self, reg, cmp_op, field, label):
        super(BRRF, self).__init__()
        self._reg = reg
        self._field = field
        self._cmp_op = cmp_op
        self._label = label

    def invoke(self, code, state):
        state = super(BRRF, self).invoke(state)
        reg_val = state.read_reg(self._reg)
        field_val, _ = state.read_header(self._field)
        cmp_val = self._cmp_op(reg_val, field_val)
        if cmp_val:
            state = self._jump_to(code, state, self._label)
        return state

class BRTR(JumpInstruction):
    """Branch control to label (label) based on whether a pattern
    in table (table) is present in the header"""
    def __init__(self, table, reg, label):
        super(BRTR, self).__init__()
        self._table_name = table.name
        self._table_fields = table.fields
        self._reg = reg
        self._label = label

    def invoke(self, code, state):
        state = super(BRTR, self).invoke(state)
        record = self._fetch_header(self._table_fields, state)
        match_found, index = state.match_table(self._table_name, record)
        if match_found:
            state.update_reg(self._reg, index)
            state = self._jump_to(code, state, self._label)
        return state

class BRTF(JumpInstruction):
    """Branch control to label (label) based on whether a pattern
    in table (table) is present in the header"""
    def __init__(self, table, field, label):
        super(BRTF, self).__init__()
        self._table_name = table.name
        self._table_fields = table.fields
        self._field = field
        self._label = label

    def invoke(self, code, state):
        state = super(BRTF, self).invoke(state)
        record = self._fetch_header(self._table_fields, state)
        match_found, index = state.match_table(self._table_name, record)
        _, validity = state.read_header(self._field)
        if match_found:
            state.update_header({self._field: (index, validity)})
            state = self._jump_to(code, state, self._label)
        return state

class IBRTR(JumpInstruction):
    """Branch control to label (label) based on whether a pattern
    in table (table) is not present in the header"""
    def __init__(self, table, reg, label):
        super(IBRTR, self).__init__()
        self._table_name = table.name
        self._table_fields = table.fields
        self._reg = reg
        self._label = label

    def invoke(self, code, state):
        state = super(IBRTR, self).invoke(state)
        record = self._fetch_header(self._table_fields, state)
        match_found, index = state.match_table(self._table_name, record)
        if not match_found:
            state = self._jump_to(code, state, self._label)
        else:
            state.update_reg(self._reg, index)
        return state

class IBRTF(JumpInstruction):
    """Branch control to label (label) based on whether a pattern
    in table (table) is not present in the header"""
    def __init__(self, table, field, label):
        super(IBRTF, self).__init__()
        self._table_name = table.name
        self._table_fields = table.fields
        self._field = field
        self._label = label

    def invoke(self, code, state):
        state = super(IBRTF, self).invoke(state)
        record = self._fetch_header(self._table_fields, state)
        match_found, index = state.match_table(self._table_name, record)
        if not match_found:
            state = self._jump_to(code, state, self._label)
        else:
            _, validity = state.read_header(self._field)
            state.update_header({self._field: (index, validity)})
        return state

class BRF(JumpInstruction):
    """Branch control to label (label) if the result of the comparison
    on field (field) and value (value) is true."""
    def __init__(self, field, cmp_op, value, label):
        super(BRF, self).__init__()
        self._cmp_op = cmp_op
        self._field = field
        self._value = value
        self._label = label

    def invoke(self, code, state):
        state = super(BRF, self).invoke(state)
        field_val, _ = state.read_header(self._field)
        cmp_val = self._cmp_op(field_val, self._value)
        if cmp_val:
            state = self._jump_to(code, state, self._label)
        return state

class BRFR(JumpInstruction):
    """Branch control to label (label) if the result of the comparison
    on field (field) and register (reg) is true."""
    def __init__(self, field, cmp_op, reg, label):
        super(BRFR, self).__init__()
        self._field = field
        self._reg = reg
        self._cmp_op = cmp_op
        self._label = label

    def invoke(self, code, state):
        state = super(BRFR, self).invoke(state)
        field_val, _ = state.read_header(self._field)
        reg_val = state.read_reg(self._reg)
        cmp_val = self._cmp_op(field_val, reg_val)
        if cmp_val:
            state = self._jump_to(code, state, self._label)
        return state

class BRFF(JumpInstruction):
    """Branch control to label (label) if the result of the comparison
    on field (field0) and field (field1) is true."""
    def __init__(self, field0, cmp_op, field1, label):
        super(BRFF, self).__init__()
        self._field0 = field0
        self._field1 = field1
        self._cmp_op = cmp_op
        self._label = label

    def invoke(self, code, state):
        state = super(BRFF, self).invoke(state)
        field0_val, _ = state.read_header(self._field0)
        field1_val, _ = state.read_header(self._field1)
        cmp_val = self._cmp_op(field0_val, field1_val)
        if cmp_val:
            state = self._jump_to(code, state, self._label)
        return state

