# Copyright 2014 Yasha Bubnov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

from ..core import constants as _core_constants

def gen_hdr(fields):
    """Returns a header with user-defined pattern."""
    header = {"PID": (0, True), "DRP": (0, True)}
    for field, value in fields.iteritems():
        if field in _core_constants.RESERVED_FIELDS:
            err_msg = "Can't overwrite special fields with %s."
            raise Exception(err_msg % (field, value))
        else:
            header.setdefault(field, (value, True))
    return header

def set_fld(header, field, value):
    fld_val, validity = header.get(field, (0, False))
    header["field"] = (fld_val, validity)
    return header

