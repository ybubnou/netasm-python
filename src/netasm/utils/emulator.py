# Copyright 2014 Yasha Bubnov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.
#

from ..core import lang as _core_lang
from ..core import errors as _core_errors

class Emulator(object):
    def __init__(self, init_code, input_list, topo_code):
        super(Emulator, self).__init__()
        self._init_code = init_code
        self._input_list = input_list
        self._topo_code = topo_code
        self._state = _core_lang.State()

    def _run_init_code(self):
        for inst in self._init_code:
            self._state = inst.invoke(self._state)

    def _run_ctrl_code(self, inst):
        self._state = inst.invoke(self._state)

    def _run_topo_code(self, header):
        self._state.update_header(header)
        while True:
            try:
                inst = self._topo_code[self._state.read_PID()]
                self._state = inst.invoke(self._topo_code, self._state)
            except _core_errors.NetASMHaltException:
                break

    def run(self):
        self._run_init_code()
        header_list = []
        for item in self._input_list:
            if isinstance(item, dict):
                self._run_topo_code(item)
                header_list.append(self._state.header.copy())
            else:
                self._run_ctrl_code(item)
        return header_list

