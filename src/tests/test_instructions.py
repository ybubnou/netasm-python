import unittest
from netasm.core import lang
from netasm.core import inst


class TestInstructions(unittest.TestCase):
    def setUp(self):
        self.table_size = 20
        self.table_fields = ["field0", "field1", "field2"]
        self.state = lang.State()
        self.table = lang.Table("tb0", self.table_size, self.table_fields)

    def test_mkr(self):
        pass

    def test_mkt(self):
        pass

    def test_wrr(self):
        pass

    def test_wrt(self):
        pass

    def test_hlt(self):
        pass

    def test_lbl(self):
        pass

    def test_ldr(self):
        pass

    def test_ldrr(self):
        pass

    def test_ldrf(self):
        pass

    def test_opr(self):
        pass

    def test_oprr(self):
        pass

    def test_oprf(self):
        pass

    def test_ldtp(self):
        pass

    def test_ldtpr(self):
        pass

    def test_ldtpf(self):
        pass

    def test_ldtf(self):
        pass

    def test_ldtfr(self):
        pass

    def test_ldtff(self):
        pass

    def test_id(self):
        pass

    def test_drp(self):
        pass

    def test_add(self):
        pass

    def test_del(self):
        pass

    def test_ldf(self):
        pass

    def test_ldfr(self):
        pass

    def test_ldff(self):
        pass

    def test_ldft(self):
        pass

    def test_ldftr(self):
        pass

    def test_ldtft(self):
        pass

    def test_opf(self):
        pass

    def test_opfr(self):
        pass

    def test_jmp(self):
        pass

    def test_brr(self):
        pass

    def test_brrr(self):
        pass

    def test_brrf(self):
        pass

    def test_brtr(self):
        pass

    def test_brtf(self):
        pass

    def test_ibrtr(self):
        pass

    def test_ibrtf(self):
        pass

    def test_btf(self):
        pass

    def test_brf(self):
        pass

    def test_brfr(self):
        pass

    def test_brff(sefl):
        pass

