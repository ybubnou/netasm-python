import unittest
from .test_instructions import TestInstructions

test_cases = (TestInstructions, )


def load_suite():
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for test_class in test_cases:
        tests = loader.loadTestsFromTestCase(test_class)
        suite.addTests(tests)
    return suite

