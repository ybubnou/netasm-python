# NetASM-python

## Description
NetASM-python is a python implementation of original NetASM project, which is available at [https://github.com/NetASM/NetASM-haskell](https://github.com/NetASM/NetASM-haskell).
NetASM is a network assembler that takes high-level SDN languages (NetKAT, P4, etc.) and maps the primitives to device-specific directives in FPGAs and various chipsets.

